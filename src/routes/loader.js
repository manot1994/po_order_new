import React from 'react'
import { Player, Controls } from '@lottiefiles/react-lottie-player';


export default function Loader() {
    return (
        <div>
            <Player
  autoplay
  loop
  src="https://assets6.lottiefiles.com/packages/lf20_0vKKEb.json"
  style={{ height: '500px', width: '500px' }}
>
  <Controls visible={false} buttons={['play', 'repeat', 'frame', 'debug']} />
</Player>
        </div>
    )
}
