import * as React from 'react'
import { BrowserRouter as Router, Redirect, Route, Switch } from 'react-router-dom'
import { useKeycloak } from '@react-keycloak/web'
import HomePage from '../pages/Home'
import LoginPage from '../pages/Login'
import { PrivateRoute } from './utils'
import Loader from '../routes/loader'
import Purchaseorder from '../components/Purchaseorder/purchaseorder'
import Createpo from '../components/Purchaseorder/Createpo'
import Supplierview from '../components/Suppliers/supplierview'
import Supplier from '../components/Suppliers/Suppliers'
import Productview from '../components/Suppliers/viewproducts'
import Supplierlogin from '../components/login'
import Invoice from '../components/Invoice/Ivoice'

export const AppRouter = () => {
  const { initialized } = useKeycloak()
  console.log('initialized',initialized)
  return (
    <>
    {
      (!initialized) ?<>
      <div><Loader/></div>
      </>:<>
      <Router>
      {/* <Navbar /> */}
        <Switch>
        <main>
        {/* <PrivateRoute path="/home" component={HomePage} /> */}
        <PrivateRoute path="/createpo" component={Createpo} />
        <PrivateRoute path="/purchaseorder" component={Purchaseorder} />
        <PrivateRoute path="/supplierview" component={Supplierview} />
        <PrivateRoute path="/Products" component={Productview} />
        <PrivateRoute path="/Supplier" component={Supplier} />
        <Route path="/login" component={LoginPage} />
        <Route path="/supplierlogin" component={Supplierlogin} />
        <Route path="/invoice" component={Invoice} />

        </main>

        </Switch>
       </Router>
      </>
    }
    </>
   
  )
}
