import React  from 'react';
import {useState} from 'react';
import axios from 'axios';
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import CloseIcon from '@material-ui/icons/Close';
import Slide from '@material-ui/core/Slide';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import TextField from '@material-ui/core/TextField';
import InputLabel from '@material-ui/core/InputLabel';
import '../Suppliers/Supplierstyle.css';
import DialogActions from '@material-ui/core/DialogActions';
import Grid from '@material-ui/core/Grid';
import { TextareaAutosize } from '@material-ui/core';
import Box from '@material-ui/core/Box';
import DateFnsUtils from '@date-io/date-fns';
import { useForm } from "react-hook-form";
import { ErrorMessage } from "@hookform/error-message";
import {MuiPickersUtilsProvider, KeyboardDatePicker} from '@material-ui/pickers';

const useStyles = makeStyles((theme) => ({
    appBar: {
        position: 'relative',
    },
    title: {
        marginLeft: theme.spacing(2),
        flex: 1,
    },
}));

const Transition = React.forwardRef(function Transition(props, ref) {
    return <Slide direction="left" ref={ref} {...props} />;
});

export default function FullScreenDialog() {
    const [area,setArea] = useState({pincode: "", city: "",  district: "", state: "", error: ""});
    const [ifsc,setIfsc] = useState({});
    const classes = useStyles();
    const [open, setOpen] = React.useState(false);
    const { register, errors, handleSubmit } =  useForm({criteriaMode: "all"});
    const onSubmit = data => console.log(data);


    const handleClickOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };

    //Tamilnadu Pincode auto fetch
    const onChange = (e) => {
        setArea({ [e.target.name]: e.target.value });

        if (e.target.value.length === 6) {
            setArea({
            error: ""
          });
          axios
            .get(`https://api.postalpincode.in/pincode/${e.target.value}`)
            .then(res =>
                setArea({
                state: res.data[0].PostOffice[0].State,
                city: res.data[0].PostOffice[0].Block,
                district: res.data[0].PostOffice[0].District
              })
            )
            .then(() => {
            })
            .catch(err => {
            });
        }
        if (e.target.value.length !== 6) {
            setArea({
            city: "",
            state: "",
            district: "",
          });
        }
      }
     //Ifsc Autofetch
     const onChangeifsc = (e) => {
        setIfsc({ [e.target.name]: e.target.value });
          axios
            .get(`https://ifsc.razorpay.com/${e.target.value}`)
            .then(res => setIfsc(res.data)
            )
            .then(() => {
            })
            .catch(err => {
            });
      }
    return (
        <div >
              <Grid container spacing={2} >
<Button variant="contained" size="small" style={{marginTop:'10px'}} color="primary" onClick={handleClickOpen}>Add Supplier</Button></Grid>
            <Dialog fullScreen open={open} onClose={handleClose} TransitionComponent={Transition} className="supplier-popup">
                <AppBar className={classes.appBar}>
                    <Toolbar>
                        <IconButton edge="start" color="inherit" onClick={handleClose} aria-label="close">
                            <CloseIcon />
                        </IconButton>
                        <Typography variant="h5" className={classes.title} align="center">Supplier Creation</Typography>
                        <Button autoFocus color="inherit" onClick={handleSubmit(onSubmit)}>Save</Button>
                    </Toolbar>
                </AppBar>
                <DialogContent>
                    <DialogContentText id="scroll-dialog-description" tabIndex={-1}>
                        <form noValidate autoComplete="off" >
                            <div className="supplier-form">
                                <Grid container spacing={3} style={{marginTop:'0px'}}>
                                    <Grid item xs={4}>                                   
                                    <InputLabel className="popup-label">Supplier Code:</InputLabel>
                                    <TextField id="outlined-basic" autoFocus name="supplierCode" type="number" variant="outlined" size="small" inputRef={register({ required: "This input is required.",maxLength: {
            value: 4,
            message: "This input must exceed 4 characters"
          } })}/>
          <ErrorMessage errors={errors} name="supplierCode" render={({ messages }) => {return messages ? Object.entries(messages).map(([type, message]) => (<p key={type} className="error-msg">{message}</p>)): null;}} />
                                    </Grid>
                                    <Grid item xs={4}>
                                        <InputLabel className="popup-label">Supplier Name:</InputLabel>
                                        <TextField id="outlined-basic" type="text" variant="outlined" size="small" name="supplierName"
        inputRef={register({required: "This input is required", maxLength: {value: 20, message: "This input must exceed 20 characters" },
          pattern:{
              value:/^[A-Za-z]+$/i,
              message: "Alphabetical characters only"
          }
        })} />
        <ErrorMessage errors={errors} name="supplierName" render={({ messages }) => {return messages ? Object.entries(messages).map(([type, message]) => (<p key={type} className="error-msg">{message}</p>)): null;}} />
                                    </Grid>
                                    <Grid item xs={4}>
                                    <InputLabel className="popup-label">Contact Number:</InputLabel>
                                    <TextField id="outlined-basic"  type="number" variant="outlined" size="small" name="contactNumber"
         />           
          <ErrorMessage errors={errors} name="contactNumber" render={({ messages }) => {return messages ? Object.entries(messages).map(([type, message]) => (<p key={type} className="error-msg">{message}</p>)): null;}} />
                                 
                                    </Grid>
                                    <Grid item xs={4}>
                                        <InputLabel className="popup-label">Mail Id:</InputLabel>
                                        <TextField id="outlined-basic" type="email" name="email" variant="outlined" size="small"  inputRef={register({required: "This input is required", pattern:{value:/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i,
        message: "Incorrect E-Mail"},
    })}/>
     <ErrorMessage errors={errors} name="email" render={({ messages }) => {return messages ? Object.entries(messages).map(([type, message]) => (<p key={type} className="error-msg">{message}</p>)): null;}} />
                                    </Grid>
                                    <Grid item xs={4}>
                                        <InputLabel className="popup-label">Pan No:</InputLabel>
                                        <TextField id="outlined-basic" type="text" name="email" variant="outlined" size="small" />
                                    </Grid>
                                    <Grid item xs={4}>
                                        <InputLabel className="popup-label">GST IN:</InputLabel>
                                        <TextField id="outlined-basic" type="text" name="email" variant="outlined" size="small" />
                                    </Grid>
                                    <Grid item xs={4}>
                                        <InputLabel className="popup-label">Pincode:</InputLabel>
                                        <TextField id="outlined-basic" type="text" name="pinCode" variant="outlined" size="small"  onChange={e => onChange(e)} value={area.pincode} inputRef={register({required: "This input is required"})}/>
                                        <ErrorMessage errors={errors} name="pinCode" render={({ messages }) => {return messages ? Object.entries(messages).map(([type, message]) => (<p key={type} className="error-msg">{message}</p>)): null;}} />
                                    </Grid>
                                    <Grid item xs={4}>
                                        <InputLabel className="popup-label">City:</InputLabel>
                                        <TextField id="outlined-basic" type="text" name="email" variant="outlined" size="small" value={area.district}/>
                                    </Grid>
                                    <Grid item xs={4}>
                                        <InputLabel className="popup-label">State:</InputLabel>
                                        <TextField id="outlined-basic" type="text" name="email" variant="outlined" size="small" value={area.state}/>
                                    </Grid>
                                    <Grid item xs={4}>
                                        <InputLabel className="popup-label">Due Date:</InputLabel>
                                        <MuiPickersUtilsProvider utils={DateFnsUtils} >
                            <Grid container justify="space-around" style={{ marginTop: '-15px' }}>
                                <KeyboardDatePicker variant="outlined"
                                    margin="normal"
                                    id="date-picker-dialog"
                                    format="MM/dd/yyyy"
                                    inputVariant="outlined"
                                    defaultValue="Small" size="small"
                                   
                                    KeyboardButtonProps={{
                                        'aria-label': 'change date',
                                    }}
                                /></Grid>
                        </MuiPickersUtilsProvider>
                                    </Grid>
                                    <Grid item xs={4}>
                                        <InputLabel className="popup-label">Bank Name:</InputLabel>
                                        <TextField id="outlined-basic" inputProps={{ maxLength: 12 }} type="text" name="email" variant="outlined" size="small" value={ifsc.BANK}/>
                                    </Grid>
                                    <Grid item xs={4}>
                                        <InputLabel className="popup-label">IFSC Code:</InputLabel>
                                        <TextField id="outlined-basic" type="text" name="email" variant="outlined" size="small"  onChange={e => onChangeifsc(e)} value={area.IFSC}  />
                                    </Grid>
                                    <Grid item xs={4}>
                                        <InputLabel className="popup-label">Account No:</InputLabel>
                                        <TextField id="outlined-basic" type="number" name="email" variant="outlined" size="small" />
                                    </Grid>
                                    <Grid item xs={4}>
                                        <InputLabel className="popup-label">Branch Name:</InputLabel>
                                        <TextField id="outlined-basic" type="text" name="email" variant="outlined" size="small" value={ifsc.BRANCH}/>
                                    </Grid>
                                </Grid>
                                <InputLabel className="popup-label" style={{marginLeft: '21px', marginTop: '27px'}}>Communication Address:</InputLabel>
                                <TextareaAutosize className="supplier-textarea" id="outlined-basic" type="text" name="email" variant="outlined" size="small" />
                            </div>
                        </form>
                    </DialogContentText>
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleClose} color="primary" variant="outlined">Cancel</Button>
                    <Button onClick={handleSubmit(onSubmit)} color="primary" variant="contained">Submit</Button>
                </DialogActions>
            </Dialog>
        </div>
    );
}
