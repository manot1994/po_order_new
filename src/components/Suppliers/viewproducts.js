import React from 'react'
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import Breadcrumbs from '@material-ui/core/Breadcrumbs';
import Typography from '@material-ui/core/Typography';
import { BrowserRouter as Router, Switch, Route, Link} from "react-router-dom";
import Paper from '@material-ui/core/Paper';
import Navbar from '../navbar'

export default function Viewproducts() {
    return (
        <div className="main-common">
          <Navbar />
            <Paper>
       <Grid container spacing={3} >
       
        <Grid item md={8} xs={6} className="breadcrumbs">
          <Breadcrumbs aria-label="breadcrumb">
      <Link color="inherit">Simple Purchase Orders</Link>
      {/* <Link color="inherit" href="/getting-started/installation/" onClick={handleClick}></Link> */}
      <Typography color="textPrimary">Product details</Typography>
    </Breadcrumbs>
        </Grid>
        <Grid item md={4} xs={6} className="breadcrumbs1">
        {/* <a href="/supplierview" style={{textDecoration:'none'}}><Button variant="outlined" size="small">Blank PO</Button></a> */}
        {/* <Button variant="outlined" size="small">Help</Button>
        <Button variant="contained" size="small" color="primary">Settings</Button> */}
        </Grid>
      </Grid>
      </Paper>
            <TableContainer style={{marginTop:'35px'}}>
    <Table aria-label="simple table">
      <TableHead style={{ padding: '6px!important', background: '#f5f5f5' }}>
        <TableRow className="supplier-table">
        <TableCell align="center" style={{ width: '10%' }}>Item Code </TableCell>
         <TableCell align="center" style={{ width: '20%' }}>Product Name</TableCell>
          <TableCell align="center" style={{ width: '10%' }}>Quantity</TableCell>
          <TableCell align="center" style={{ width: '10%' }}>Storage</TableCell>
          <TableCell align="center" style={{ width: '15%' }}>Color</TableCell>
          <TableCell align="center" style={{ width: '10%' }}>Price</TableCell>
          <TableCell align="center" style={{ width: '10%' }}>Tax</TableCell>
          <TableCell align="center" style={{ width: '15%' }}></TableCell>
        </TableRow>
      </TableHead>
      <TableBody style={{background:'#ffff'}}>
        <TableRow className="supplier-table1">
          <TableCell align="center">001</TableCell>
          <TableCell align="center">Samsung Galaxy S21 Plus 5G</TableCell>
          <TableCell align="center">100</TableCell>
          <TableCell align="center">8GB-128GB</TableCell>
          <TableCell align="center">Phantom Silver</TableCell>
          <TableCell align="center">85,000</TableCell>
          <TableCell align="center">100</TableCell>
          <TableCell align="center"><Button size="small" variant="contained" color="default">Create Po</Button></TableCell>
        </TableRow> 
        <TableRow className="supplier-table1">
          <TableCell align="center">001</TableCell>
          <TableCell align="center">Samsung Galaxy S21 Plus 5G</TableCell>
          <TableCell align="center">100</TableCell>
          <TableCell align="center">8GB-128GB</TableCell>
          <TableCell align="center">Phantom Silver</TableCell>
          <TableCell align="center">85,000</TableCell>
          <TableCell align="center">100</TableCell>
          <TableCell align="center"><Link to="/createpo" style={{textDecoration:'none'}}><Button size="small" variant="contained" color="default">Create Po</Button></Link></TableCell>
        </TableRow>
        <TableRow className="supplier-table1">
          <TableCell align="center">001</TableCell>
          <TableCell align="center">Samsung Galaxy S21 Plus 5G</TableCell>
          <TableCell align="center">100</TableCell>
          <TableCell align="center">8GB-128GB</TableCell>
          <TableCell align="center">Phantom Silver</TableCell>
          <TableCell align="center">85,000</TableCell>
          <TableCell align="center">100</TableCell>
          <TableCell align="center"><Button size="small" variant="contained" color="default">Create Po</Button></TableCell>
        </TableRow>
        <TableRow className="supplier-table1">
          <TableCell align="center">001</TableCell>
          <TableCell align="center">Samsung Galaxy S21 Plus 5G</TableCell>
          <TableCell align="center">100</TableCell>
          <TableCell align="center">8GB-128GB</TableCell>
          <TableCell align="center">Phantom Silver</TableCell>
          <TableCell align="center">85,000</TableCell>
          <TableCell align="center">100</TableCell>
          <TableCell align="center"><Button size="small" variant="contained" color="default">Create Po</Button></TableCell>
        </TableRow>
        <TableRow className="supplier-table1">
          <TableCell align="center">001</TableCell>
          <TableCell align="center">Samsung Galaxy S21 Plus 5G</TableCell>
          <TableCell align="center">100</TableCell>
          <TableCell align="center">8GB-128GB</TableCell>
          <TableCell align="center">Phantom Silver</TableCell>
          <TableCell align="center">85,000</TableCell>
          <TableCell align="center">100</TableCell>
          <TableCell align="center"><Button size="small" variant="contained" color="default">Create Po</Button></TableCell>
        </TableRow>
        <TableRow className="supplier-table1">
          <TableCell align="center">001</TableCell>
          <TableCell align="center">Samsung Galaxy S21 Plus 5G</TableCell>
          <TableCell align="center">100</TableCell>
          <TableCell align="center">8GB-128GB</TableCell>
          <TableCell align="center">Phantom Silver</TableCell>
          <TableCell align="center">85,000</TableCell>
          <TableCell align="center">100</TableCell>
          <TableCell align="center"><Button size="small" variant="contained" color="default">Create Po</Button></TableCell>
        </TableRow>
        <TableRow className="supplier-table1">
          <TableCell align="center">001</TableCell>
          <TableCell align="center">Samsung Galaxy S21 Plus 5G</TableCell>
          <TableCell align="center">100</TableCell>
          <TableCell align="center">8GB-128GB</TableCell>
          <TableCell align="center">Phantom Silver</TableCell>
          <TableCell align="center">85,000</TableCell>
          <TableCell align="center">100</TableCell>
          <TableCell align="center"><Button size="small" variant="contained" color="default">Create Po</Button></TableCell>
        </TableRow>
      </TableBody>
    </Table>
  </TableContainer>
        </div>
    )
}
