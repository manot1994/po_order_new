import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Button from '@material-ui/core/Button';
import ButtonGroup from '@material-ui/core/ButtonGroup';
import Box from '@material-ui/core/Box';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import DeleteIcon from '@material-ui/icons/Delete';


const useStyles = makeStyles({
  table: {
    minWidth: 650,
  },
});

function createData(name, calories, fat, carbs, protein) {
  return { name, calories, fat, carbs, protein };
}

const rows = [
  createData('GHOST1350', '2020-04-20', 'Neff', 'N/A', 'Items to transfer'),
  createData('1908', '2020-01-10', 'Simple PO test', '#1098', 'Not Completed'),
  createData('GHOST1350', '2020-04-20', 'Neff', 'N/A', 'Not Completed'),
  createData('1906', '2020-04-20', 'Neff', '#1907', 'Items to transfer'),
  createData('1907', '2020-04-20', 'Simple PO test', 'N/A', 'Items to transfer'),
  createData('GHOST1350', '2020-04-20', 'Neff', 'N/A', 'Not Completed'),
  createData('1906', '2020-04-20', 'Neff', '#1907', 'Items to transfer'),
  createData('1907', '2020-04-20', 'Simple PO test', 'N/A', 'Items to transfer'),
  createData('GHOST1350', '2020-04-20', 'Neff', 'N/A', 'Not Completed'),
  createData('1906', '2020-04-20', 'Neff', '#1907', 'Items to transfer'),
  createData('1907', '2020-04-20', 'Simple PO test', 'N/A', 'Items to transfer'),
];

export default function BasicTable() {
  const classes = useStyles();
  const [age, setAge] = React.useState('');

  const handleChange = (event) => {
    setAge(event.target.value);
  };
  const [state, setState] = React.useState({
    checkedA: true,
    checkedB: true,
    checkedF: true,
    checkedG: true,
  });

  const checkboxhandleChange = (event) => {
    setState({ ...state, [event.target.name]: event.target.checked });
  };
  return (
    <Box>
      <Box display="flex" justifyContent="flex-end" mb={1} >
        <Box style={{ margin: '4px' }}>Delete POs older than</Box>
        <FormControl variant="outlined" className={classes.formControl}>
          {/* <InputLabel id="demo-simple-select-outlined-label">Age</InputLabel> */}
          <Select className="olderpo-select" style={{ margin: '4px' }}
            labelId="demo-simple-select-outlined-label"
            id="demo-simple-select-outlined"
            value={age}
            onChange={handleChange}
          >
            <MenuItem value="">
              <em>None</em>
            </MenuItem>
            <MenuItem value={10}>1 Month</MenuItem>
            <MenuItem value={20}>2 Month</MenuItem>
            <MenuItem value={30}>3 Month</MenuItem>
          </Select>
        </FormControl>
        <Button size="small" variant="contained" color="secondary" style={{ margin: '4px' }} startIcon={<DeleteIcon />}
        >
          Delete POs
      </Button>
      </Box>

      <TableContainer component={Paper} className="po-table">
        <Table className={classes.table} aria-label="simple table">
          <TableHead style={{ padding: '6px!important', background: '#f5f5f5' }}>
            <TableRow className="status">
              <TableCell align="center" style={{ width: '1%' }}> <FormControlLabel control={<Checkbox name="checkedB" onChange={checkboxhandleChange} color="primary" />} /></TableCell>
              <TableCell align="center" style={{ width: '15%' }}>PO</TableCell>
              <TableCell align="center" style={{ width: '15%' }}>Date</TableCell>
              <TableCell align="center" style={{ width: '15%' }}>Supplier</TableCell>
              <TableCell align="center" style={{ width: '15%' }}>Order Number</TableCell>
              <TableCell align="center" style={{ width: '20%' }} >Status</TableCell>
              <TableCell align="center" style={{ width: '19%' }}></TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {rows.map((row) => (
              <TableRow key={row.name}>
                <TableCell component="th" scope="row" align="center"><FormControlLabel control={<Checkbox onChange={checkboxhandleChange} name="checkedB" color="primary" />} /></TableCell>
                <TableCell component="th" scope="row" align="center">{row.name}</TableCell>
                <TableCell align="center">{row.calories}</TableCell>
                <TableCell align="center">{row.fat}</TableCell>
                <TableCell align="center">{row.carbs}</TableCell>
                <TableCell align="center"><Button size="small" className={classes.margin} variant="contained" style={{ background: 'rgb(220 203 0 / 74%)' }}>{row.protein}</Button></TableCell>
                <TableCell align="center"><ButtonGroup color="primary" aria-label="outlined primary button group"><a href="/orderdetail"><Button variant="contained" color="primary" size="small">View</Button></a> <Button variant="contained" color="primary" size="small">Complete</Button><Button size="small">Delete</Button>
                </ButtonGroup></TableCell>

              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    </Box>

  );
}
