import React from 'react'
import Grid from '@material-ui/core/Grid';
import Breadcrumbs from '@material-ui/core/Breadcrumbs';
import Link from '@material-ui/core/Link';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import FlagIcon from '@material-ui/icons/Flag';
import '../Purchaseorder/purchase.css'
import Tab from '../Tab/tab'
import Supplier from '../Suppliers/Suppliers'


export default function purchaseorder() {
  return (
    <Grid container>
       <Grid container spacing={3} >

        <Grid item md={8} xs={6} className="breadcrumbs">
          <Breadcrumbs aria-label="breadcrumb">
      <Link color="inherit" href="/" >Simple Purchase Orders</Link>
      {/* <Link color="inherit" href="/getting-started/installation/" onClick={handleClick}></Link> */}
      <Typography color="textPrimary">Welcome</Typography>
    </Breadcrumbs>
        </Grid>
        <Grid item md={4} xs={6} className="breadcrumbs1">
        <a href="/createpo" style={{textDecoration:'none'}}><Button variant="outlined" size="small">Blank PO</Button></a>
        <Button variant="outlined" size="small">Help</Button>
        <Button variant="contained" size="small" color="primary">Settings</Button>
        </Grid>
      </Grid>
      <Grid container className="Breadcrumbs-head"><FlagIcon color="primary"/>  You can now setup Simple PO to send emails from email address.<Link>click here to set up</Link> </Grid>
      <Grid container style={{padding:'12px!important'}}> 
      <Supplier/>
      </Grid>
     
    </Grid>
  )
}
