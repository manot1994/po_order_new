import * as React from 'react'
import { useCallback } from 'react'

import { useKeycloak } from '@react-keycloak/web'
import type { KeycloakInstance } from 'keycloak-js'

import { useAxios } from '../utils/hooks'

export default () => {
  const { keycloak } = useKeycloak<KeycloakInstance>()

  const axiosInstance = useAxios('http://localhost:3000') // see https://github.com/panz3r/jwt-checker-server for a quick implementation
  const callApi = useCallback(() => {
    !!axiosInstance.current && axiosInstance.current.get('/jwt/decode')
  }, [axiosInstance])

  return (
    <div>
      {/* <div>User is authenticated</div> */}
      {!!keycloak?.authenticated && (
        <div  onClick={() => keycloak.logout()}>
          Logout
        </div>
      )}

      {/* <button type="button" onClick={callApi}>
        Call API
      </button> */}
    </div>
  )
}
