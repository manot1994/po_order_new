import * as React from 'react'
import { useCallback, useEffect} from 'react'
import { Redirect, withRouter } from 'react-router-dom'

import { useKeycloak } from '@react-keycloak/web'
import type { KeycloakInstance } from 'keycloak-js'

const LoginPage = withRouter(({ location }) => {
  const currentLocationState = {
    from: { pathname: '/home' }
  }
  const { keycloak } = useKeycloak<KeycloakInstance>()
  console.log('keycloak',keycloak)
  const login = useCallback(() => {
    keycloak?.login()
  }, [keycloak])
  useEffect(() => {
    // Update the document title using the browser API
    login()
  });
  if (keycloak?.authenticated)
    return <Redirect to={currentLocationState.from} />
  return (
    <div>
        Login Page
    </div>
  )
})
export default LoginPage
